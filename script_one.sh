#!/bin/bash
# A bash script that returns the third and then the first passed arguments
# Arguments from bash command line
args=("$@")

if [ ${#args[@]} -eq  0 ];  then 
     echo "There are were passed no arguments"
else
     echo ${args[2]}
     echo ${args[0]}
fi


