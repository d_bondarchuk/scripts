#!/bin/bash
# A script that returns all arguments except the third if more than four arguments were passed. 
# Otherwise, script returns all passed arguments
# Arguments from bash command line
args=("$@")

if test ${#args[@]} -gt  4
then 
     echo ${args[@]:0:2} ${args[@]:3}

else
     echo ${args[@]}
fi

