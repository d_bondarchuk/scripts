QASTD-31

1) script_one.sh - a bash script that returns the third and then the first passed arguments, if no arguments are passed error message will be shown.

2) script_two.sh - a script that returns all arguments except the third, if passed more than four arguments. Otherwise, the script returns all arguments.
